package ru.t1.sarychevv.tm.api.controller;

public interface ITaskController {

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();
}
