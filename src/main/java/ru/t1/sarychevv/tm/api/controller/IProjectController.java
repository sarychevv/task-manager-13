package ru.t1.sarychevv.tm.api.controller;

public interface IProjectController {

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

}
