package ru.t1.sarychevv.tm.repository;

import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task create(final String name,final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        tasks.remove(task);
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Integer getSize() {
        return tasks.size();
    }

}
